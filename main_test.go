package main

import "testing"

func TestHello(t *testing.T) {
	s := Hello("Drone")
	e := "Hello Drone !\n"
	if s != e {
		t.Fail()
	}
}